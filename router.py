"""
Маршрутизатор
"""
from flask import Flask
from flask import render_template as flask_render_template
from flask import redirect
from flask import request
from flask import abort
from db import DB
from models.news import NewsRepository
from models.news import News
from models.user import UserRepository
from models.user_create_form import UserCreateForm
from models.auth import Auth
from api.v1 import init as init_api_v1


# Инициализирует маршруты для маршрутизатора и определяет обработчики запросов
def init_route(app: Flask, db: DB):
    # Инициализация зависимостей для обработчиков
    news_repository = NewsRepository(db)
    user_repository = UserRepository(db)
    auth = Auth(user_repository)

    # Переопределение стандартного рендера, добавляет параметр auth_user
    def render_template(*args, **kwargs):
        kwargs['auth_user'] = auth.get_user()
        return flask_render_template(*args, **kwargs)

    #Инициализация маршутов для API
    init_api_v1(app, news_repository, auth)

    @app.route('/')
    @app.route('/index')
    def index():
        last_users = user_repository.get_last(3)
        last_news = news_repository.get_last(3)
        return render_template(
            'index.html',
            title='Новый новостной портал',
            last_users=last_users,
            last_news=last_news
        )

    @app.route('/install')
    def install():
        news_repository.install()
        user_repository.install()
        return render_template(
            'install-success.html',
            title="Главная"
        )

    @app.route('/login', methods=['GET', 'POST'])
    def login():
        has_error = False
        login = ''
        if request.method == 'POST':
            login = request.form['login']
            if auth.login(login, request.form['password']):
                return redirect('/')
            else:
                has_error = True
        return render_template(
            'login.html',
            title='Вход',
            login=login,
            has_error=has_error
        )

    @app.route('/logout', methods=['GET'])
    def logout():
        auth.logout()
        return redirect('/')

    @app.route('/user', methods=['GET'])
    def user_list():
        user_list = user_repository.get_list()
        return render_template(
            'user-list.html',
            title='Пользователи',
            user_list=user_list
        )

    @app.route('/user/<int:id>', methods=['GET'])
    def user_view(id: int):
        user = user_repository.get_by_id(id)
        user_news = news_repository.get_by_user_id(user.id)
        return render_template(
            'user-view.html',
            title='Пользователь - ' + user.login,
            user=user,
            user_news=user_news
        )

    @app.route('/user/create', methods=['GET', 'POST'])
    def user_create_form():
        form = UserCreateForm(user_repository)
        if form.validate_on_submit():
            form.create_user()
            return redirect('/user')
        return render_template(
            'user-create.html',
            title='Создать пользователя',
            form=form
        )

    @app.route('/user/delete/<int:id>')
    def user_delete(id: int):
        user = user_repository.get_by_id(id)
        user_repository.delete(user)
        return redirect('/user')

    @app.route('/news', methods=['GET'])
    def news_list():
        news_list = news_repository.get_list()
        return render_template(
            'news-list.html',
            title="Новости",
            news_list=news_list
        )

    @app.route('/news/create', methods=['GET'])
    def news_create_form():
        return render_template(
            'news-create.html',
            title="Новая новость"
        )

    @app.route('/news/create', methods=['POST'])
    def news_create():
        if not auth.is_authorized():
            return redirect('/login')
        news = News(None, request.form['title'], request.form['content'], auth.get_user().id)
        news_repository.create(news)
        return redirect('/news')

    @app.route('/news/<int:id>')
    def news_view(id: int):
        news = news_repository.get_by_id(id)
        user = user_repository.get_by_id(news.user_id)
        return render_template(
            'news-view.html',
            title='Новость - ' + news.title,
            news=news,
            user=user
        )

    @app.route('/news/delete/<int:id>')
    def news_delete(id: int):
        news = news_repository.get_by_id(id)
        if news.user_id != auth.get_user().id:
            abort(403)
        news_repository.delete(news)
        return redirect('/news')
